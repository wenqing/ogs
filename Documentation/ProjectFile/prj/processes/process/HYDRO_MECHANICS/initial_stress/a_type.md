An optional attribute to indicate the type of the initial stress, `total` or
 `effective`. The default value is `effective`.
